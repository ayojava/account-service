Installation Instructions
=========================
# Database Setup
### Create Database Schema in Mysql
 schema name = account-service-db

### Review the file `application.yaml` edit the following , under 'datasource':
```properties
url = 
username =
password = 

```
### Build  the application 

mvn clean package

### Run  the application 

mvn spring-boot:run

*** Populate the DB by running this script below

```sql

insert into AccountEntity(id,accountNo,accountBalance) values (1,'11111111' , 8000);
insert into AccountEntity(id,accountNo,accountBalance) values (2,'22222222' , 7000);

```

### Launch swagger url

http://localhost:7118/account-service/swagger-ui.html

```
Account Numbers : 11111111, 22222222
Account Balance : 8000 , 7000
```

*** Test case automatically populates the db with its test script