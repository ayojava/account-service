create table AccountEntity (id bigint not null primary key, accountNo   varchar(255) not null,accountBalance NUMBER(10,0) not null);
create table TransactionEntity (
    id bigint not null primary key,
    transactionType   varchar(255) not null,
    otherAccount   varchar(255) not null,
    createDate  datetime null,
    accountEntity_id       bigint   null,
    CONSTRAINT fk_TXNEMP FOREIGN KEY (id) REFERENCES AccountEntity (id)
    );
--insert into AccountEntity(id,accountNo,accountBalance) values (1,'11111111' , 8000);
--ßinsert into AccountEntity(id,accountNo,accountBalance) values (2,'22222222' , 7000);