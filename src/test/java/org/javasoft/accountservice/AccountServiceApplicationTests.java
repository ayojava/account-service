package org.javasoft.accountservice;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.javasoft.accountservice.entity.AccountEntity;
import org.javasoft.accountservice.payload.TransferMoneyRequest;
import org.javasoft.accountservice.repository.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.javasoft.accountservice.api.ClientAPI.ACCOUNT_V1_API;
import static org.javasoft.accountservice.api.ClientAPI.TRANSFER_MONEY;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpStatus.OK;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:startUp.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:shutDown.sql") })
public class AccountServiceApplicationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private AccountRepository accountRepository;

    private double initialCreditAccountBalance , initialDebitAccountBalance;

    private AccountEntity creditAccount , debitAccount;

    private double transferAmount ;

    @Before
    public void setUp() throws Exception {
        transferAmount = 2500;
        creditAccount = new AccountEntity();
        creditAccount.setAccountBalance(8000d);
        creditAccount.setAccountNo("11111111");
        debitAccount = new AccountEntity();
        debitAccount.setAccountBalance(7000d);
        debitAccount.setAccountNo("22222222");

        initialCreditAccountBalance = accountRepository.save(creditAccount).getAccountBalance();
        initialDebitAccountBalance = accountRepository.save(debitAccount).getAccountBalance();
    }

    @Test
    public void testtransfer() throws Exception{
        final val transferMoneyRequest = buildTransferMoneyRequest();
        final val testRestTemplate = new TestRestTemplate();
        final val responseEntity = testRestTemplate
                .postForEntity(buildUrl(TRANSFER_MONEY), transferMoneyRequest, TransferMoneyRequest.class);
        assertEquals(OK,responseEntity.getStatusCode());

        final val creditAccountEntity_ = accountRepository.findByAccountNo(creditAccount.getAccountNo()).get();
        final val debitAccountEntity_ = accountRepository.findByAccountNo(debitAccount.getAccountNo()).get();

        assertThat(creditAccountEntity_.getAccountBalance()).isEqualTo(initialCreditAccountBalance + transferAmount);
        assertThat(debitAccountEntity_.getAccountBalance()).isEqualTo(initialDebitAccountBalance - transferAmount);
    }

    private String buildUrl(String requestPath){
        final val stringBuilder = new StringBuilder();
        return stringBuilder.append("http://localhost:")
                .append(port)
                .append(ACCOUNT_V1_API)
                .append(requestPath)
                .toString();
    }
    private TransferMoneyRequest buildTransferMoneyRequest(){
        final val transferMoneyRequest = new TransferMoneyRequest();
        transferMoneyRequest.setCreditAccount(creditAccount.getAccountNo());
        transferMoneyRequest.setDebitAccount(debitAccount.getAccountNo());
        transferMoneyRequest.setTransferAmount(transferAmount);
        return transferMoneyRequest;
    }
}
