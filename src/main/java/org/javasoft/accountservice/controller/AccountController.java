package org.javasoft.accountservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.accountservice.facade.AccountFacade;
import org.javasoft.accountservice.payload.AccountDetailsResponse;
import org.javasoft.accountservice.payload.TransferMoneyRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.javasoft.accountservice.api.ClientAPI.*;

@Slf4j
@RestController
@RequestMapping(ACCOUNT_V1_API)
@Api(value ="AccountController" ,description = "Set of endpoints for Account Services.")
public class AccountController {

    private final AccountFacade accountFacade;

    @Autowired
    public AccountController(AccountFacade accountFacade) {
        this.accountFacade = accountFacade;
    }

    @ResponseBody
    @RequestMapping(value = TRANSFER_MONEY, method = RequestMethod.POST)
    @ApiOperation(value = TRANSFER_MONEY, notes = "Transfer Money from one Account to another")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No content", response = String.class),
            @ApiResponse(code = 400, message = "Bad Request", response = String.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = String.class)
    })
    @ResponseStatus(HttpStatus.OK)
    public void transferMoney(@Valid @RequestBody TransferMoneyRequest transferMoneyRequest){
        accountFacade.transferMoney(transferMoneyRequest);
    }

    @ResponseBody
    @RequestMapping(value = ACCOUNT_TRANSACTIONS ,produces = "application/json" , method = RequestMethod.GET)
    @ApiResponses({
            @ApiResponse(code = 204, message = "No content", response = String.class),
            @ApiResponse(code = 400, message = "Bad Request", response = String.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = String.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = String.class)
    })
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Retrieve Account Details ", response = AccountDetailsResponse.class)
    public AccountDetailsResponse getAccountDetails(@PathVariable("accountNo") String  accountNo){
        return accountFacade.findAccountDetails(accountNo);
    }
}
