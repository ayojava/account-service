package org.javasoft.accountservice.payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class AccountDetailsResponse {

    @ApiModelProperty(notes = "The Account No")
    private String accountNo;

    @ApiModelProperty(notes ="The Account Balance "  )
    private String accountBalance;

    @ApiModelProperty(notes ="Transaction History "  )
    private List<TransactionHistory> transactionHistory;
}
