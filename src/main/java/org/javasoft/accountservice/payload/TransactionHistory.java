package org.javasoft.accountservice.payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TransactionHistory {

    @ApiModelProperty(notes ="Amount"  )
    private String amount ;

    @ApiModelProperty(notes ="Transaction Date "  )
    private String transactionDate;

    @ApiModelProperty(notes ="Destination Account "  )
    private String destinationAccount;

    @ApiModelProperty(notes ="Transaction Type "  )
    private String transactionType;
}
