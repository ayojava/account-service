package org.javasoft.accountservice.payload;

import lombok.Data;

@Data

public class TransferMoneyRequest {

    private String debitAccount;

    private String creditAccount;

    private double transferAmount;
}
