package org.javasoft.accountservice.entity;

import lombok.Data;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@Entity
@Table(name = "AccountEntity")
public class AccountEntity extends AbstractPersistable<Long> {

    @NotBlank
    private String accountNo;

    private Double accountBalance;

    @JoinColumn(name = "accountId")
    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private List<TransactionEntity> transactionEntityList;
}
