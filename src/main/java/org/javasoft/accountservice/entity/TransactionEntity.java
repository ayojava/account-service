package org.javasoft.accountservice.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@Entity
@Table(name = "TransactionEntity")
public class TransactionEntity extends AbstractPersistable<Long> {

    @NotBlank
    private String transactionType;

    private Double transactionAmount;

    @NotBlank
    private String otherAccount;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;
}
