package org.javasoft.accountservice.util;

public interface TxnUtil {

    String DEBIT ="Debit";

    String CREDIT ="Credit";

    String DATE_PATTERN ="dd-MM-yyyy";
}
