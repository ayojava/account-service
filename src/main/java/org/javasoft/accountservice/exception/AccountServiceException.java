package org.javasoft.accountservice.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountServiceException extends RuntimeException{

    private String errorMessage;
}
