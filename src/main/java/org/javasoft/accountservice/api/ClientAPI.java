package org.javasoft.accountservice.api;

public interface ClientAPI {

    String ACCOUNT_V1_API="/v1/account";

    String TRANSFER_MONEY = "/transferMoney";

    String ACCOUNT_TRANSACTIONS = "/{accountNo}";
}
