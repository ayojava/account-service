package org.javasoft.accountservice.facade;

import lombok.val;
import org.javasoft.accountservice.entity.AccountEntity;
import org.javasoft.accountservice.entity.TransactionEntity;
import org.javasoft.accountservice.exception.AccountServiceException;
import org.javasoft.accountservice.mapper.AccountMapper;
import org.javasoft.accountservice.payload.AccountDetailsResponse;
import org.javasoft.accountservice.payload.TransferMoneyRequest;
import org.javasoft.accountservice.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.javasoft.accountservice.util.TxnUtil.CREDIT;
import static org.javasoft.accountservice.util.TxnUtil.DEBIT;

@Component
public class AccountFacade {

    private final AccountRepository accountRepository;

    private final AccountMapper accountMapper;

    @Autowired
    public AccountFacade(AccountRepository accountRepository, AccountMapper accountMapper) {
        this.accountRepository = accountRepository;
        this.accountMapper = accountMapper;
    }

    public AccountDetailsResponse findAccountDetails(String accountNo){
        final val optAccountEntity = accountRepository.findByAccountNo(accountNo);
        final val accountEntity = optAccountEntity
                .orElseThrow(() -> new AccountServiceException("Account [" + accountNo + "] Not Found"));
        return accountMapper.mapAccountEntity.apply(accountEntity);
    }

    @Transactional
    public void transferMoney(TransferMoneyRequest transferMoneyRequest){
        final val creditAccount = transferMoneyRequest.getCreditAccount();
        final val debitAccount = transferMoneyRequest.getDebitAccount();
        final val transferAmount = transferMoneyRequest.getTransferAmount();

        final val optCreditAccountEntity = accountRepository.findByAccountNo(creditAccount);
        final val creditAccountEntity = optCreditAccountEntity
                .orElseThrow(() -> new AccountServiceException("Credit Account [" + creditAccount + "] Not Found"));

        final val optDebitAccountEntity = accountRepository.findByAccountNo(debitAccount);
        final val debitAccountEntity = optDebitAccountEntity
                .orElseThrow(() -> new AccountServiceException("Debit Account [" + debitAccount + "] Not Found"));

        if(debitAccountEntity.getAccountBalance() < transferAmount){
            throw new AccountServiceException(" Insufficient Account Balance ");
        }

        //Debit Entries
        final val newDebitAccountBalance = debitAccountEntity.getAccountBalance() - transferAmount;
        final val debitTxnEntity = new TransactionEntity();
        debitTxnEntity.setOtherAccount(creditAccount);
        debitTxnEntity.setTransactionAmount(transferAmount);
        debitTxnEntity.setTransactionType(DEBIT);
        debitAccountEntity.setAccountBalance(newDebitAccountBalance);
        List<TransactionEntity> debitTxnEntityList = debitAccountEntity.getTransactionEntityList();
        debitTxnEntityList = debitTxnEntityList==null ? new ArrayList<>() : debitTxnEntityList;
        debitTxnEntityList.add(debitTxnEntity);
        debitAccountEntity.setTransactionEntityList(debitTxnEntityList);

        //Credit Entries
        final val newCreditAccountBalance = creditAccountEntity.getAccountBalance() + transferAmount;
        final val creditTxnEntity = new TransactionEntity();
        creditTxnEntity.setOtherAccount(debitAccount);
        creditTxnEntity.setTransactionAmount(transferAmount);
        creditTxnEntity.setTransactionType(CREDIT);
        creditAccountEntity.setAccountBalance(newCreditAccountBalance);
        List<TransactionEntity> creditTxnEntityList = creditAccountEntity.getTransactionEntityList();
        creditTxnEntityList = creditTxnEntityList==null ? new ArrayList<>() : creditTxnEntityList;
        creditTxnEntityList.add(creditTxnEntity);
        creditAccountEntity.setTransactionEntityList(creditTxnEntityList);

        accountRepository.save(debitAccountEntity);
        accountRepository.save(creditAccountEntity);
    }
}
