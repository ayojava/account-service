package org.javasoft.accountservice.mapper;

import lombok.val;
import org.apache.commons.lang.time.DateFormatUtils;
import org.javasoft.accountservice.entity.AccountEntity;
import org.javasoft.accountservice.payload.AccountDetailsResponse;
import org.javasoft.accountservice.payload.TransactionHistory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.javasoft.accountservice.util.TxnUtil.DATE_PATTERN;

@Component
public class AccountMapper {

    public Function<AccountEntity , AccountDetailsResponse> mapAccountEntity = accountEntity -> {
        final val accountDetailsResponse = new AccountDetailsResponse();
        accountDetailsResponse.setAccountNo(accountEntity.getAccountNo());
        accountDetailsResponse.setAccountBalance(String.valueOf(accountEntity.getAccountBalance()));

        List<TransactionHistory> transactionHistoryList = accountEntity.getTransactionEntityList().stream()
                .map(transactionEntity -> {
                    final val txnHistory = new TransactionHistory();
                    txnHistory.setAmount(String.valueOf(transactionEntity.getTransactionAmount()));
                    txnHistory.setDestinationAccount(transactionEntity.getOtherAccount());
                    txnHistory.setTransactionDate(DateFormatUtils.format(transactionEntity.getTransactionDate(), DATE_PATTERN));
                    txnHistory.setTransactionType(transactionEntity.getTransactionType());
                    return txnHistory;
                })
                .collect(Collectors.toList());
        accountDetailsResponse.setTransactionHistory(transactionHistoryList);
        return accountDetailsResponse;
    };
}
